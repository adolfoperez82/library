<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repository\ElementRepositoryInterface as RepositoryInterface;
use App\Repository\Implementations\ElementRepositoryImplementation as RepositoryImplementation;

class ElementRepositoryServiceProvider extends ServiceProvider
{

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
      $this->app->singleton(RepositoryInterface::class,  RepositoryImplementation::class);
  }
}
