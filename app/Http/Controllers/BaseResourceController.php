<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $repository;
    protected $code;
    protected $message;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $elements = $this->repository->index();
        if($elements){
            $this->code = 200;
            $this->message = $elements->toArray();
        } else if($elements === false){
            $this->code = 500;
            $this->message = 'error';
        }
        
        $data = [
            'code' => $this->code,
            'message' => $this->message,
        ];

        return json_encode($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->getArrayAssocFromRequestJson($request);
        $errors = $this->repository->isValid($data, $request->method());
        if(empty($errors)){
            $instance = $this->repository->add($data);
            if(!$instance){
                $this->code = 500;
                $this->message = 'error';
            } else {
                $this->code = 200;
                $this->message = 'successfull';
            }
        } else {
            $this->code = 400;
            $this->message = $errors;
        }

        $data = [
            'code' => $this->code,
            'message' => $this->message,
        ];

        return json_encode($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $element = $this->repository->load($id);
        if($element){
            $this->code = 200;
            $this->message = $element->toArray();
        } else if($element === null){
            $this->code = 404;
            $this->message = 'not found';
        } else if($element === false){
            $this->code = 500;
            $this->message = 'error';
        }
        
        $data = [
            'code' => $this->code,
            'message' => $this->message,
        ];

        return json_encode($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $data = $this->getArrayAssocFromRequestJson($request);
        $errors = $this->repository->isValid($data, $request->method());
     
        if(empty($errors)){
            $instance = $this->repository->update($data, $id);
            if(!$instance){
                $this->code = 500;
                $this->message = 'error';
            } else {
                $this->code = 200;
                $this->message = 'successfull';
            }
        } else {
            $this->code = 400;
            $this->message = $errors;
        }

        $data = [
            'code' => $this->code,
            'message' => $this->message,
        ];

        return json_encode($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->repository->destroy($id);
        if($result){
            $this->code = 500;
            $this->message = 'error';
        } else {
            $this->code = 200;
            $this->message = 'successfull';
        }
        $data = [
            'code' => $this->code,
            'message' => $this->message,
        ];

        return json_encode($data);
    }

    /**
     * Search the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $data = $this->getArrayAssocFromRequestJson($request);
        $elements = $this->repository->search($data);
        if($elements){
            $this->code = 200;
            $this->message = $elements->toArray();
        } else if($elements === null){
            $this->code = 404;
            $this->message = 'not found';
        } else if($elements === false){
            $this->code = 500;
            $this->message = 'error';
        }        
        $data = [
            'code' => $this->code,
            'message' => $this->message,
        ];

        return json_encode($data);
    }

    private function getArrayAssocFromRequestJson(){
        return json_decode(request()->getContent(), true);
    }
}
