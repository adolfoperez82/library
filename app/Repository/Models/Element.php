<?php
namespace App\Repository\Models;

class Element extends BaseModel
{

    protected $table = 'elements';
    protected $fillable = [
        'id', 'name', 'group_id', 'code',
    ];
    
    protected $validationRulesNew = [
        'name' => 'required|min:2|max:50',
        'group_id' => 'required',
        'code' => 'unique:elements'
    ];

    protected $validationRulesUpdate = [
        'name' => 'unique:elements|min:2|max:50',
        'group_id' => 'required',
        'code' => 'unique:elements'
    ];

    public function group()
    {
        return $this->belongsTo('\App\Repository\Models\Group', 'group_id', 'id');
    }

    public function user()
    {
        return $this->belongsToMany('\App\User', 'bookings', 'element_id', 'user_id');
    }
}
