<?php

namespace App\Repository;

interface BaseRepositoryInterface {
    
    public function index();

    public function load($id);

    public function add($data);

    public function update($data, $id);

    public function destroy($id);

    public function search(Request $request);
}