<?php

namespace App\Repository\Implementations;

use App\Repository\GroupRepositoryInterface as BaseInterface;
use App\Repository\Implementations\BaseRepositoryImplementation as BaseImpl;
use App\Repository\Models\Group as EloquentModel;

class GroupRepositoryImplementation extends BaseImpl implements BaseInterface {

    public function __construct() {
        $this->eloquentModel = new EloquentModel();
    }
}
