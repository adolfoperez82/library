<?php

namespace App\Repository\Implementations;

use App\Repository\ElementRepositoryInterface as RepositoryInterface;
use App\Repository\Implementations\BaseRepositoryImplementation as BaseImplement;
use App\Repository\Models\Element as EloquentModel;

class ElementRepositoryImplementation extends BaseImplement implements RepositoryInterface {

    public function __construct() {
        $this->eloquentModel = new EloquentModel();
    }
}