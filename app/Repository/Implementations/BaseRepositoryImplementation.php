<?php

namespace App\Repository\Implementations;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\RelationNotFoundException;

class BaseRepositoryImplementation
{

    protected $eloquentModel;

    public function index(){
        try{
            $instance = $this->eloquentModel->all();
        }catch(\Illuminate\Database\QueryException $ex){ 
            return false;
        }
        return $instance;
    }

    public function load($id){
        try{
            $instance = $this->eloquentModel->find($id);
        }catch(\Illuminate\Database\QueryException $ex){ 
            return false;
        }
        return $instance;
    }

    public function add($data)
    {
        try{
            $instance = $this->eloquentModel->insert($data);
        }catch(\Illuminate\Database\QueryException $ex){ 
            return false;
        }
        return $instance;

    }

    public function update($data, $id)
    {
        try{
            $instance = $this->eloquentModel->find($id);
            $instance->update($data);
        }catch(\Illuminate\Database\QueryException $ex){ 
            return false;
        }
        return $instance;

    }

    public function destroy($id){
        try{
            $this->eloquentModel->destroy($id);
        }catch(\Illuminate\Database\QueryException $ex){ 
            return true;
        }
    }

    public function isValid($data, $method){
        $validator = Validator::make($data, $this->eloquentModel->getValidationRules($method));
        if($validator->fails()){
            return $validator->errors();
        } else {
            return null;
        }
    }

    public function search($data){
        $filters = $data['filters'] ?? null;
        $with = $data['with'] ?? null;
        $query = $this->eloquentModel;
        if(!empty($filters)){
            foreach($filters as $filter){
                $query = $query->where($filter[0], $filter[1], $filter[2]);
            }
        }
        if(!empty($with)){
            $query=$query->with($with);
        }

        try{
            $results = $query->get();
        }catch(\Illuminate\Database\QueryException $ex){ 
            return null;
        } catch (RelationNotFoundException $ex) {
            return false;
        }
        return $results;
    }
    
}